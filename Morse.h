/*
 * Morse.h
 *
 *  Created on: Jan 18, 2017
 *      Author: roland
 */

#ifndef MORSE_H_
#define MORSE_H_

#include <Arduino.h>

class Morse
{
public:
    Morse(uint16_t pin, uint16_t diTime);

    void sendString(const char* string);

    uint16_t getDiTime();
    void setDiTime(uint16_t diTime);

private:
    static const uint8_t _specialCount;

    struct SpecialChar
	{
    	char ch;
    	const char* morse;
	};

    void send(const char* morse);

    void sendDi(void);
    void sendDah(void);

    static const char* _letters[];
    static const char* _digits[];
    static const SpecialChar _special[];

    uint16_t _pin;
    uint32_t _diTime;
    uint32_t _dahTime;
    uint32_t _spaceTime;
    uint32_t _letSpaceTime;
    uint32_t _wordSpaceTime;
};


#endif /* MORSE_H_ */
