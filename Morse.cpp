/*
 * Morse.cpp
 *
 *  Created on: Jan 18, 2017
 *      Author: roland
 */

#include "Morse.h"

const uint8_t Morse::_specialCount = 16;

const char* Morse::_letters[] = {".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....", "..", ".---", "-.-", ".-..", "--", "-.", "---", ".--.", "--.-", ".-.", "...", "-", "..-", "...-", ".--", "-..-", "-.--", "--.."};
const char* Morse::_digits[] = {"-----", ".----", "..---", "...--", "....-", ".....", "-....", "--...", "---..", "----."};
//                                  .         ,          '         _
//const char* Morse::_special[] = {".-.-.-", "--..--", ".----.", "..--.-"};
const Morse::SpecialChar Morse::_special[Morse::_specialCount] = {
		{'.', ".-.-.-"},
		{',', "--..--"},
		{'\'', ".----."},
		{'"', ".-..-."},
		{'_', "..--.-"},
		{':', "---..."},
		{';', "-.-.-."},
		{'?', "..--.."},
		{'!', "-.-.--"},
		{'-', "-....-"},
		{'+', ".-.-."},
		{'/', "-..-."},
		{'(', "-.--."},
		{')', "-.--.-"},
		{'=', "-...-"},
		{'@', ".--.-."}
};

Morse::Morse(uint16_t pin, uint16_t diTime)
    : _pin(pin), _diTime(diTime), _dahTime(diTime * 3), _spaceTime(diTime), _letSpaceTime(diTime * 2), _wordSpaceTime(diTime * 4)
{
    pinMode(_pin, OUTPUT);
}

void Morse::sendString(const char* string)
{
    while(*string != '\0')
    {
        char letter = tolower(*string);

        if(letter == ' ')
        {
        	delay(_wordSpaceTime);
        }
        else if(letter >= 'a' && letter <= 'z')
        {
            send(_letters[letter - 'a']);
        }
        else if(letter >= '0' && letter <= '9')
        {
            send(_digits[letter - '0']);
        }
        else
        {
        	for(uint8_t i = 0; i < _specialCount; ++i)
        	{
        		if(_special[i].ch == letter)
        		{
        			send(_special[i].morse);
        			break;
        		}
        	}
        }

        ++string;
    }
}

uint16_t Morse::getDiTime()
{
	return _diTime;
}

void Morse::setDiTime(uint16_t diTime)
{
	_diTime = diTime;
	_dahTime = diTime * 3;
	_spaceTime = diTime;
	_letSpaceTime = diTime * 2;
	_wordSpaceTime = diTime * 4;
}

void Morse::send(const char* morse)
{
    while(*morse != '\0')
    {
        if(*morse == '.')
            sendDi();
        else
            sendDah();

        ++morse;
    }
    delay(_letSpaceTime);
}

void Morse::sendDi(void)
{
    digitalWrite(_pin, HIGH);
    delay(_diTime);
    digitalWrite(_pin, LOW);
    delay(_spaceTime);
}

void Morse::sendDah(void)
{
    digitalWrite(_pin, HIGH);
    delay(_dahTime);
    digitalWrite(_pin, LOW);
    delay(_spaceTime);
}



